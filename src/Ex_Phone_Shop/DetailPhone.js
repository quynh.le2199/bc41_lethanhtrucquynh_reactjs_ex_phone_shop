import React, { Component } from 'react'

export default class DetailPhone extends Component {
    render() {
      let {hinhAnh, tenSP, manHinh, heDieuHanh, cameraTruoc, cameraSau, ram, rom} = this.props.detail;
    return (
      <div className='row border border-primary py-4 mb-5'>
        <div className="col-6">
            <h3>{tenSP}</h3>
            <img src={hinhAnh} alt="" style={{width:'80%'}}/>
        </div>
        <div className="col-6">
            <table className='table text-left'>
                <thead>
                    <tr>
                        <td><b>Thông số kỹ thuật</b></td>
                    </tr>
                    <tr>
                        <td>Màn hình</td>
                        <td>{manHinh}</td>
                    </tr>
                    <tr>
                        <td>Hệ điều hành</td>
                        <td>{heDieuHanh}</td>
                    </tr>
                    <tr>
                        <td>Camera trước</td>
                        <td>{cameraTruoc}</td>
                    </tr>
                    <tr>
                        <td>Camera sau</td>
                        <td>{cameraSau}</td>
                    </tr>
                    <tr>
                        <td>RAM</td>
                        <td>{ram}</td>
                    </tr>
                    <tr>
                        <td>ROM</td>
                        <td>{rom}</td>
                    </tr>
                </thead>
            </table>
        </div>
      </div>
    )
  }
}
