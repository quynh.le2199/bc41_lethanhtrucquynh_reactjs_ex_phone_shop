import React, { Component } from 'react'
import { data_phone } from './data_phone'
import ListPhone from './ListPhone'
import DetailPhone from './DetailPhone'

export default class Ex_Phone_Shop extends Component {
  state = {
    listPhone: data_phone,
    detail: data_phone[0]
  }
  handleDetail = (idPhone) => {
    let id = idPhone;
    this.setState({detail: data_phone[id]})
  }
  render() {
    return (
      <div className='container'>
        <h2>Ex_Phone_Shop</h2>
        <ListPhone list={this.state.listPhone} handleDetail={this.handleDetail}/>
        <DetailPhone detail={this.state.detail}/>
      </div>
    )
  }
}
