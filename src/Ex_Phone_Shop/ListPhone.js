import React, { Component } from 'react'
import ItemPhone from './ItemPhone'

export default class ListPhone extends Component {
    renderListPhone = () => {
      return this.props.list.map((item, index) => {
        return <ItemPhone phone={item} key={index} handleDetail={this.props.handleDetail}/>
      })
    }
  render() {
    return (
      <div className='row'>
        {this.renderListPhone()}
      </div>
    )
  }
}
